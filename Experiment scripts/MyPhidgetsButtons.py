﻿
#Basic imports
import vizact
import viz
from ctypes import *
import sys
import random
#Phidget specific imports
from Phidgets.PhidgetException import PhidgetErrorCodes, PhidgetException
from Phidgets.Events.Events import AttachEventArgs, DetachEventArgs, ErrorEventArgs, InputChangeEventArgs, OutputChangeEventArgs, SensorChangeEventArgs
from Phidgets.Devices.InterfaceKit import InterfaceKit

#Event Handler Callback Functions
def inferfaceKitAttached(e):
	attached = e.device
	print("InterfaceKit %i Attached!" % (attached.getSerialNum()))

def interfaceKitDetached(e):
	detached = e.device
	print("InterfaceKit %i Detached!" % (detached.getSerialNum()))

def interfaceKitError(e):
    try:
        source = e.device
        print("InterfaceKit %i: Phidget Error %i: %s" % (source.getSerialNum(), e.eCode, e.description))
    except PhidgetException as e:
        print("Phidget Exception %i: %s" % (e.code, e.details))

#function that detects input changes and triggers events
def interfaceKitInputChanged(e):
	source = e.device
	print("InterfaceKit %i: Input %i: %s" % (source.getSerialNum(), e.index, e.state))
	##PP1 has input 1 (blue-white cable), PP2 has input 0 (red-black cable)
	#Low (or TRUE), and High (or FALSE)
	if source.getInputState(1)==1:
		print('Digital input '+str(1)+' is True, button pressed')
		viz.sendEvent(DIP1_TRUE,e)
	else:
		print ('Digital input '+str(1)+' is False, button released')
		viz.sendEvent(DIP1_FALSE,e)
		
	if source.getInputState(0)==1:
		print('Digital input '+str(0)+' is True, button pressed')
		viz.sendEvent(DIP0_TRUE,e)
	else:
		print ('Digital input '+str(0)+' is False, button released')
		viz.sendEvent(DIP0_FALSE,e)


def setPhidgetsHandlers():
	global interfaceKit
	try:
		interfaceKit.setOnAttachHandler(inferfaceKitAttached)
		interfaceKit.setOnDetachHandler(interfaceKitDetached)
		interfaceKit.setOnErrorhandler(interfaceKitError)
		interfaceKit.setOnInputChangeHandler(interfaceKitInputChanged)
	except PhidgetException as e:
		print("Phidget Exception %i: %s" % (e.code, e.details))
		print("Exiting....")
		exit(1)

def initPhidgets():
	#create events and assign them unique ID names
	global DIP1_TRUE, DIP0_TRUE
	DIP1_TRUE= viz.getEventID('DIP1_TRUE')
	print DIP1_TRUE
	DIP0_TRUE= viz.getEventID('DIP0_TRUE')
	print DIP0_TRUE
	global DIP1_FALSE, DIP0_FALSE
	DIP1_FALSE= viz.getEventID('DIP1_FALSE')
	DIP0_FALSE= viz.getEventID('DIP0_FALSE')
	global interfaceKit
	#Create an interfacekit object
	try:
		interfaceKit = InterfaceKit()
	except RuntimeError as e:
		print("Runtime Exception: %s" % e.details)
		print("Exiting....")
		exit(1)
	print("Opening phidget object....")

	try:
		interfaceKit.openPhidget()
	except PhidgetException as e:
		print("Phidget Exception %i: %s" % (e.code, e.details))
		print("Exiting....")
		exit(1)

	print("Waiting for attach....")

	try:
		interfaceKit.waitForAttach(10000)
	except PhidgetException as e:
		print("Phidget Exception %i: %s" % (e.code, e.details))
		try:
			interfaceKit.closePhidget()
		except PhidgetException as e:
			print("Phidget Exception %i: %s" % (e.code, e.details))
			print("Exiting....")
			exit(1)
		print("Exiting....")
		exit(1)
	print("Setting the data rate for each sensor index to 16ms....")
	for i in range(interfaceKit.getSensorCount()):
		try:
			interfaceKit.setDataRate(i, 16)
		except PhidgetException as e:
			print("Phidget Exception %i: %s" % (e.code, e.details))
	setPhidgetsHandlers()

def closePhidgets():
	global interfaceKit
	print("Closing...")

	try:
		interfaceKit.closePhidget()
	except PhidgetException as e:
		print("Phidget Exception %i: %s" % (e.code, e.details))
		print("Exiting....")
		exit(1)