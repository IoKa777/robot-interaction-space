﻿#basic imports
import sys
import time
from datetime import datetime 
from datetime import timedelta
import random
import math
import winsound

#viz specific imports
import viz
import vizact
import vizinput
import myviztracker as viztracker

#nao specific imports
import naoqi
import nao

#phidget import
import MyPhidgetsButtons

#globals
global pos1, pos2, t1, t2, DIP1, DIP0

def onKeyDown(key):
	if key == viz.KEY_END:#this participant is ready
		viz.callback(viz.KEYDOWN_EVENT,None)
		viz.callback(viz.TIMER_EVENT, None)
		startSession()
	elif key==viz.KEY_ESCAPE:
		onKeyDown("q")
		vizinput.message("The file "+fileout.name+" is saved")
		fileout.close()
		vizinput.message("End of experiment")
	#wizard of oz
	elif key=="i":
		print "Robot goes to initial position"
		nao.InitPose()
	elif key=="w":
		print "Robot starts walking"
		dx=1
		dy=0
		dtheta=0	
		frequency=1
		nao.motionProxy.setWalkTargetVelocity(dx, dy, dtheta, frequency)
	elif key=="s":
		print "Nao stops walking"
		dtheta=0
		dx=0
		dy=0
		frequency=1
		nao.motionProxy.setWalkTargetVelocity(dx, dy, dtheta, frequency)
	elif key=="c":
		print "Nao goes to crouch position"
		# crouch pose
		dtheta=0
		dx=0
		dy=0
		freq=1
		nao.Move(dx, dy, dtheta, freq)
		time.sleep(0.1)
		
		nao.Crouch()
#		pNames = "Body" # after crouch stiffness is always removed
#		pStiffnessLists = 0.0
#		pTimeLists = 1.0
#		nao.motionProxy.stiffnessInterpolation(pNames, pStiffnessLists, pTimeLists)
	elif key=="r":
		print "Nao says could you please rate this path of approach?"
		nao.Say("Could you please rate this direction of approach?")
	elif key=="q":
		print "Nao says this is the end of the experiment."
		nao.Say("This is the end of this interaction. Could you please fill in the last questionnaire? Thanks for participating!")
	
		
def buttonPressed(index):
	global DIP1, DIP0, pos1, pos2, t1, t2
	print "Handler accessed"
	if index==1:
		print "it is the stupid input 1"
		DIP1=True
		t1=time.time()-tstart
		pos1=markerA.getPosition()
		print pos1
	if index==0:
		print "it is stupid input 0"
		DIP0=True
		t2=time.time()-tstart
		pos2=markerA.getPosition()
		

def dist() :
	pass
	
def check_input(timer_id):
	#first check robot position
	global trial
	global tstart
	global nbrConditions
	global pos1, pos2, t1, t2,tstop,posstop,stdelay,dx1,dz1,dx2,dz2,d1,d2
	curr_pos=markerA.getPosition()
	dx=curr_pos[0]#-xv[samplenbr]
	dz=curr_pos[2]#-zv[samplenbr]
	d=math.sqrt(dx*dx+dz*dz)
	print dx, dz, d
	
	if d>0.35 and dz>0.30: #safe distance
		if DIP1 and DIP0:
			print "Both inputs TRUE, robot should stop now"
			viz.killtimer(0)# kill timer, we do not need any input check now
			stdelay=random.choice([0.75, 1.75, 2.75])
			time.sleep(stdelay)
			onKeyDown("s")
			onKeyDown("c")
			timestop=time.time()-tstart
			time.sleep(1)
			onKeyDown("r")
			#calculate distance for pp1
			dx1=pos1[0]#-xv[samplenbr]
			dz1=pos1[2]#-zv[samplenbr]
			#distance from pp1 not from reference point
			d1=math.sqrt((dx1-0.6)*(dx1-0.6)+dz1*dz1)
			print pos1 , d1
			#calculate distance for pp2
			dx2=pos2[0]#-xv[samplenbr]
			dz2=pos2[2]#-zv[samplenbr]
			#distance from pp2 not from reference point
			d2=math.sqrt((dx2+0.6)*(dx2+0.6)+dz2*dz2)
			print pos2,  d2
			#calculate distance from reference when the robot actually stops
			posstop=markerA.getPosition()
			dxstop=posstop[0]#-xv[samplenbr]
			dzstop=posstop[2]
			dstop=math.sqrt(dxstop*dxstop+dzstop*dzstop)
			tstop=time.time()-tstart
			
#			fileout.write(str(samplenbr+1)+" "+str(round(t-tstart,3))+" dx1= "+str(round(dx1,3))+" dz1= "+str(round(dz1,3))+" d1= "+str(round(d1,3))++" dx2= "+str(round(dx2,3))+" dz2= "+str(round(dz2,3))+" d2= "+str(round(d2,3))+"\n")
			fileout.write(str(trial  )
			        +"\t"+str(direction[trial-1])
					+"\t"+str(plays_first[trial-1])
					+"\t"+str(round(dx1,3 ))
					+"\t"+str(round(dz1,3 ))
					+"\t"+str(round(d1,3  ))
					+"\t"+str(round(t1,3))
					+"\t"+str(round(dx2,3 ))
					+"\t"+str(round(dz2,3 ))
					+'\t'+str(round(d2,3 ))
					+"\t"+str(round(t2,3))
					+"\t"+str(round(stdelay,3 ))
					+"\t"+str(round(dxstop,3 ))
					+"\t"+str(round(dzstop,3 ))
					+'\t'+str(round(dstop,3 ))
					+"\t"+str(round(tstop,3))
					+"\n")
			fileout.flush()
			textScreen.message(str(trial  )
			        +"\t"+str(direction[trial - 1])
					+"\t"+str(plays_first[trial - 1])
					+"\t"+str(round(dx1,3 ))
					+"\t"+str(round(dz1,3 ))
					+"\t"+str(round(d1,3  ))
					+"\t"+str(round(t1,3))
					+"\t"+str(round(dx2,3 ))
					+"\t"+str(round(dz2,3 ))
					+'\t'+str(round(d2,3 ))
					+"\t"+str(round(t2,3))
					+"\t"+str(round(stdelay,3 ))
					+"\t"+str(round(dxstop,3 ))
					+"\t"+str(round(dzstop,3 ))
					+'\t'+str(round(dstop,3 ))
					+"\t"+str(round(tstop,3)))
			if trial==nbrConditions:
				#viz.callback(viz.KEYDOWN_EVENT,None)
				vizinput.message("The file "+fileout.name+" is saved")
#				textScreen.message("End of experiment")
				textScreen.message("End of experiment? Wait for experimenter's ESCAPE key")
			else:
				textScreen.message("End of condition, press END_KEY")
#			else:
#				tf.setEnabled(viz.ON)#enable anti-bouncing timer
					
		elif DIP1:
			print "Only DIP1 TRUE"
		
		elif DIP0:
			print "Only DIP0 TRUE"
		else:
			pass
	else: #not both pressed a button, so stop robot when too close
		print "d<0.35"
		onKeyDown("s")
		onKeyDown("c")
		onKeyDown("r")
		#calculate distance from reference when the robot actually stops
		posstop=markerA.getPosition()
		dxstop=posstop[0]#-xv[samplenbr]
		dzstop=posstop[2]
		dstop=math.sqrt(dxstop*dxstop+dzstop*dzstop)
		tstop=time.time()-tstart
		if DIP1:
			#calculate distance for pp1
			dx1=pos1[0]#-xv[samplenbr]
			dz1=pos1[2]#-zv[samplenbr]
			d1=math.sqrt((dx1-0.6)*(dx1-0.6)+dz1*dz1)
			print pos1, d1
			print "DIP1 true"
		if DIP0:
			#calculate distance for pp2
			dx2=pos2[0]#-xv[samplenbr]
			dz2=pos2[2]#-zv[samplenbr]
			d2=math.sqrt((dx2+0.6)*(dx2+0.6)+dz2*dz2)
			print pos2, d2
			print "DIP0 true"
		viz.killtimer(0)# kill timer, we do not need any input check now
		fileout.write(str(trial)
				+"\t"+str(direction[trial-1])
				+"\t"+str(plays_first[trial-1])
				+"\t"+str(dx1  )
				+"\t"+str(dz1 )
				+"\t"+str(d1 )
				+"\t"+str(round(t1,3))
				+"\t"+str(round(dx2,3 ))
				+"\t"+str(round(dz2,3 ))
				+'\t'+str(round(d2,3 ))
				+"\t"+str(round(t2,3))
				+"\t"+str(round(stdelay,3 ))
				+"\t"+str(round(dxstop,3 ))
				+"\t"+str(round(dzstop,3 ))
				+'\t'+str(round(dstop,3 ))
				+"\t"+str(round(tstop,3))
				+"\n")
		fileout.flush()
		textScreen.message(str(trial)
			        +"\t"+str(direction[trial-1])
					+"\t"+str(plays_first[trial-1])
					+"\t"+str(round(dx1,3 ))
					+"\t"+str(round(dz1,3 ))
					+"\t"+str(round(d1,3  ))
					+"\t"+str(round(t1,3))
					+"\t"+str(round(dx2,3 ))
					+"\t"+str(round(dz2,3 ))
					+'\t'+str(round(d2,3 ))
					+"\t"+str(round(t2,3))
					+"\t"+str(round(stdelay,3 ))
					+"\t"+str(round(dxstop,3 ))
					+"\t"+str(round(dzstop,3 ))
					+'\t'+str(round(dstop,3 ))
					+"\t"+str(round(tstop,3)))
			
		#if samplenbr1==nbrConditions:
		if trial==nbrConditions:
			vizinput.message("The file "+fileout.name+" is saved")
#			textScreen.message("End of experiment")
			textScreen.message("End of experiment? Wait for experimenter's ESCAPE key")
		else:
			textScreen.message("End of condition, press END_KEY")

def initialize():
	global DIP1, DIP0
	global robot
	global nbrConditions
	global direction
	global plays_first
	global path
	global tf
	global textScreen
	global trial
	global markerA
	global pos1, pos2, t1, t2,stdelay,dx1,dx2
	global tstart
	global pp1
	global pp2
	global fileout
	
	#initialize nao
#	robotIP="192.168.1.105" #nao @ Virtu/e
	robotIP="192.168.1.103" #Marvin @ Virtu/e

	try:
		nao.InitProxy(robotIP, [1])#initialize ALTextToSpeechProxy
	except Exception, e:
		print "Could not create proxy to ALTextToSpeech"
		print "Error was: ", e
			
	
	try:
		nao.InitProxy(robotIP, [2])#initialize AudioDevice
	except Exception, e:
		print "Could not create proxy to ALAudioDevice"
		print "Error was: ", e
	
	try:
		nao.InitProxy(robotIP, [3])#initialize motionProxy
	except Exception, e:
		print "Could not create proxy to ALMotion"
		print "Error was: ", e
	
	nao.tts.setVoice('Kenny22Enhanced')
	
	#initialize phidget
	MyPhidgetsButtons.initPhidgets()
	DIP1=False	
	DIP0=False
	textScreen = viz.addText('Initializing robotpos',viz.SCREEN)
	textScreen.fontSize(50)
	
	#block interrupts during initialization
	viz.callback(viz.KEYDOWN_EVENT,None)
	viz.callback(viz.TIMER_EVENT, None)
	
	#Creates marker for measuring data
	phasespace = viz.add('phasespace.dle',0,'192.168.1.100')
	markerA = phasespace.addMarker() #create marker for the robot
	#pos1=[-999, -999, -999]
	#pos1=[-999, -999, -999]
	
	vpname=vizinput.fileOpen()
	filein=open(vpname,'r')
	line=filein.readline()
	nbrConditions=int(line)
	direction=range(100)
	plays_first=range(100)
	for i in range(nbrConditions):
		line=filein.readline()
		s=line.split()
		direction[i]=s[0]
		plays_first[i]=s[1]
	filein.close()
	path = vizinput.directory(prompt='Select a folder to save the position files.')
	
	trial=0
#	tf=vizact.ontimer(1.0,onTimer)
#	tf.setEnabled(viz.OFF)#disable it for now
	print "Initialization phase complete!"
	tstart=time.time()
	pp1=vizinput.input("Participant's 1 identifier: ")
	pp2=vizinput.input("Participant's 2 identifier: ")
	fileout = open(path+"/"+pp1+"_"+pp2+"_"+time.strftime('%y%m%d%H%M%S',time.localtime())+".pos", 'w') # outputfile
	if trial==0:
		fileout.write('trial\tdirection\tplays_first\tdx1\tdz1\td1\tt1\tdx2\tdz2\td2\tt2\tdelay\tdxstop\tdzstop\tdstop\ttstop\n')
	
def startSession():
	global pp1,pp2
	global trial
	trialQuestion="Previous no. trial= "+str(trial)+". What is the current no. trial?"
	tr=vizinput.input(trialQuestion)
	trial=int(tr)
	resetGlobals()
	
	
	names=['HeadYaw', 'HeadPitch', 'LShoulderPitch', 'LShoulderRoll', 'LElbowYaw', 'LElbowRoll', 'LWristYaw', 'LHand', 
	'LHipYawPitch', 'LHipRoll', 'LHipPitch', 'LKneePitch', 'LAnklePitch', 'LAnkleRoll', 'RHipYawPitch', 'RHipRoll', 'RHipPitch', 
	'RKneePitch', 'RAnklePitch', 'RAnkleRoll', 'RShoulderPitch', 'RShoulderRoll', 'RElbowYaw', 'RElbowRoll', 'RWristYaw', 'RHand']
	stiffnessLists=[1.0, 1.0,0.0, 1.0,1.0, 1.0,1.0, 1.0,1.0, 1.0,1.0, 1.0,1.0, 1.0,1.0, 1.0,1.0, 1.0,1.0, 1.0,0.0, 1.0,1.0, 1.0,1.0, 1.0]
	nao.motionProxy.stiffnessInterpolation(names,stiffnessLists,1)#avoid overheating
	
	#winsound.Beep(1000,500) # participants know when the game starts
	nao.PlaySine(500,10,1,2)
	time.sleep(30) # should be 45
	nao.InitPose()
	onKeyDown("w")
	
	viz.starttimer(0,0.05,viz.FOREVER)#triggers event every 50msec
	viz.callback(MyPhidgetsButtons.DIP1_TRUE, buttonPressed)
	viz.callback(MyPhidgetsButtons.DIP0_TRUE, buttonPressed)
	viz.callback(viz.KEYDOWN_EVENT,onKeyDown)
	viz.callback(viz.TIMER_EVENT, check_input)
	
def resetGlobals():
	global DIP1, DIP0, pos1, pos2, t1, t2,tstop,posstop,stdelay,dx1,dz1,dx2,dz2,d1,d2
	DIP1=False
	DIP0=False
	pos1=[0, -1, 0]
	pos2=[0, -1, 0]
	posstop=[0, -1, 0]
	t1=0
	t2=0
	tstop=0
	stdelay=0
	dx1=0
	dz1=0
	dx2=0
	dz2=0
	d1=0
	d2=0
	
	
	
##MAIN
initialize()
startSession()
viz.go()		



		
