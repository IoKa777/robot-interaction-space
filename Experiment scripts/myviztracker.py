import viz
import __main__

"""
Implement the following functions so they return the appropriate objects.
Each function should be implemented as a generator, so you must "yield" the
tracker objects, not "return" them.
"""

# Define your PORTs here:
#__main__.PORT_X = 123

"""
Setup graphics window, stereo mode, projection settings, etc..
"""
def go(flag=0,**kw):
	viz.go(flag)

"""
Add a position tracker object
"""
def resetPos():
	global link
	global pos
	posm=pos.getPosition()
	link.setOffset([-posm[0],-posm[1],-posm[2]])
	print posm
	print link.getPosition()
	
def _addPosTracker():
	import vizact
	global link
	global marker
	global pos
	phasespace = viz.add('phasespace.dle',0,'192.168.1.100')# zonder master als primary client
#	phasespace = viz.add('phasespace.dle',1,'192.168.1.100')# als slave van master
	pos = phasespace.addMarker()
	#print phasespace
	returnedNode=viz.addGroup()
	link=viz.link(pos,returnedNode,priority=-5)
	vizact.onkeydown('p',resetPos)
	yield returnedNode

def _addOriTracker():

# INTERSENSE ORIENTATION (3DOF ORI)
	# R-key reset
	
	import vizact
	ori = viz.add('intersense.dls')
	
	#create link for reset and scale operations
	returnedNode = viz.addGroup()
	link = viz.link(ori, returnedNode, priority=-5) #early priority link
	
	#orientation reset
	vizact.onkeydown('r', ori.reset)
	
	yield returnedNode #return linkable node to calling code
"""
Add a position and orientation tracker object
"""
def _addTracker():
#	viz.logError('** ERROR: viztracker.add() has not been implemented')
#	yield None
	
	yield Keyboard6DOF()


"""
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
DO NOT MODIFY CODE BELOW
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"""

def _CameraTracker(handler):
	import vizcam
	view = viz.addView()
	view.eyeheight(0)
	man = vizcam.Manager(view=view,handler=handler)
	return view
	
def KeyboardMouse6DOF(**kw):
	"""Add a 6DOF keyboard tracker"""
	import vizcam
	vizcam.AUTO_REGISTER = 0
	return _CameraTracker(vizcam.FlyNavigate(**kw))

def Keyboard6DOF(**kw):
	"""Add a 6DOF keyboard tracker"""
	import vizcam
	vizcam.AUTO_REGISTER = 0
	return _CameraTracker(vizcam.KeyboardCamera(**kw))

def KeyboardPos(**kw):
	"""Add a position keyboard tracker"""
	import vizcam
	vizcam.AUTO_REGISTER = 0
	return _CameraTracker(vizcam.KeyboardPosCamera(**kw))

def KeyboardOri(**kw):
	"""Add an orientation keyboard tracker"""
	import vizcam
	vizcam.AUTO_REGISTER = 0
	return _CameraTracker(vizcam.KeyboardOriCamera(**kw))
	
def MouseTrack(**kw):
	import vizcam
	vizcam.AUTO_REGISTER = 0
	return _CameraTracker(vizcam.DefaultNavigate(**kw))

def MousePos(**kw):
	"""Add a position mouse tracker"""
	return MouseTracker(**kw)

_data = viz.Data()
_data.posGen = _addPosTracker()
_data.oriGen = _addOriTracker()
_data.trackerGen = _addTracker()
_data.options = {}

def addPos(**kw):
	"""Add a position tracker object"""
	_data.options = kw
	try:
		return _data.posGen.next()
	except StopIteration:
		viz.logError('** ERROR: viztracker has no more position trackers')
		return None

def addOri(**kw):
	"""Add an orientation tracker object"""
	_data.options = kw
	try:
		return _data.oriGen.next()
	except StopIteration:
		viz.logError('** ERROR: viztracker has no more orientation trackers')
		return None

def add(**kw):
	"""Add a 6DOF tracker object"""
	_data.options = kw
	try:
		return _data.trackerGen.next()
	except StopIteration:
		viz.logError('** ERROR: viztracker has no more trackers')
		return None
		
class MouseTracker(viz.VizNode,viz.EventClass):
	def __init__(self,**kw):
		import vizact
		viz.EventClass.__init__(self)		
		node = viz.addGroup()		
		viz.VizNode.__init__(self,node.id)		
		viz.EventClass.callback(self,viz.UPDATE_EVENT,self.onUpdate)		
		self.length = 10
		vizact.onwheeldown(self.scroll,-.25)
		vizact.onwheelup(self.scroll,.25)
	def scroll(self,num):
		self.length += num
	def onUpdate(self,e):
		sPos = viz.Mouse.getPosition()
		line = viz.screentoworld(sPos)
		line.length=self.length
		self.setPosition(line.end)




"""
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
USE THE TEMPLATES BELOW TO BUILD YOUR OWN TRACKERS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""
 

"""=====================Start copy below=====================
	# KEYBOARD NAVIGATION (6DOF)
	# Gamestyle a/s/w/d
	
	yield Keyboard6DOF()
======================End copy above====================="""

 
"""=====================Start copy below=====================
	# INTERSENSE ORIENTATION (3DOF ORI)
	# R-key reset
	
	import vizact
	ori = viz.add('intersense.dls')
	
	#create link for reset and scale operations
	returnedNode = viz.addGroup()
	link = viz.link(ori, returnedNode, priority=-5) #early priority link
	
	#orientation reset
	vizact.onkeydown('r', ori.reset)
	
	yield returnedNode #return linkable node to calling code
======================End copy above====================="""
	
	
"""=====================Start copy below=====================
	# WORLDVIZ PPT / INTERSENSE HYBRID (6DOF)
	# R-key reset ori
	# T-key reset pos (x & z only)
	# Q/W/E pos scale factor (1x, 2x, 3x)
	# Eyeheight set to 0.0

	import vizact
	
	pos = viz.add('vizppt.dls')
	if not pos.valid():
		pos = viz.addGroup()
	ori = viz.add('intersense.dls')
	
	sixDOF = viz.mergeLinkable(pos, ori)
	
	#create link for reset and scale operations
	returnedNode = viz.addGroup()
	link = viz.link(sixDOF, returnedNode, enabled=False)
	vizact.onupdate(viz.PRIORITY_PLUGINS+1,link.update) #Manually update link after plugins
	#orientation reset
	vizact.onkeydown('r', ori.reset)
	
	vizact.onkeydown('t', link.reset, viz.RESET_X | viz.RESET_Z) #position reset
	
	positionScale = link.postScale([1,1,1],target=viz.LINK_POS_OP) #Scale movement factor
	vizact.onkeydown('q', positionScale.setScale, [1,1,1])
	vizact.onkeydown('w', positionScale.setScale, [2,1,2])
	vizact.onkeydown('e', positionScale.setScale, [3,1,3])
	
	viz.eyeheight(0)
	yield returnedNode #return linkable node to calling code
======================End copy above====================="""


"""=====================Start copy below=====================
	# POLHEMUS FASTRAK (6DOF)
	# Auto-hemisphere tracking
	# Sensor needs to be in the transmitter's +Z (Vizard's 
	# +Y) hemisphere BOTH when turning on control unti AND
	# first time starting Vizard simulation using this device.
	
	__main__.PORT_FASTRAK = 5  # Set this to correct COM port #

	fastrak = viz.add('fastrak.dls')
	yield fastrak
======================End copy above====================="""
