﻿# robotpos: A Vizard application for logging position info of the NAO robot with respect to the participant's position.
# Copyright 2012, Technische Universiteit Eindhoven, IE&IS section Human Technology Interaction
# Author: Martin C. Boschman
# Date: May 23,  2012
# Version 1.0
# Use: Run within the Vizard application in combination with the PhaseSpace positioning system in the VirTU/e lab.
# Prerequisites: - Input text file "viewerpos.txt" with the x and z position coordinates of the participant's location: 
#                  1 line with 2 floats separated by a space character. 
#                  This file should be placed in the same directory where the application file resides.
#                - One PhaseSpace LED labeled "A" should be attached to the head of the NAO robot. The LED should be 
#                  connected to a functioning driver unit. 
#                - The participant holds a button which should be connected to one digital input of a Phidgets interface. 
#                  The latter should be connected to a USB interface of the computer from which this application is running.
import viz
import vizact
import vizinput
import time
import myviztracker as viztracker
import MyPhidgetsButtons
import math

# Callback functions
def onKeyDown(key):#experimenter's control by keystrokes
	if key == viz.KEY_END:#this participant is ready
		viz.callback(MyPhidgetsButtons.PHIDGETS_BUTTON_PRESSED,None)
		viz.callback(viz.KEYDOWN_EVENT,None)
		vizinput.message("The file "+fileout.name+" is saved")
		fileout.close()
		startSession()
	if key==viz.KEY_ESCAPE:#all session are finished, exit the program
		vizinput.message("The file "+fileout.name+" is saved")
		fileout.close()
		vizinput.message("Ready!")
		
def buttonPressed(e):# handler for the Phidgids button
	global samplenbr
	global tstart
	global nbrConditions
	viz.callback(MyPhidgetsButtons.PHIDGETS_BUTTON_PRESSED,None)
	t=time.time()
	samplenbr=samplenbr+1
	pos=robot.getPosition()
	dx=pos[0]-xv[samplenbr-1]
	dz=pos[2]-zv[samplenbr-1]
	print xv[samplenbr-1]
	print zv[samplenbr-1]
	
	d=math.sqrt(dx*dx+dz*dz)
	fileout.write(str(samplenbr)+" "+str(round(t-tstart,3))+" "+str(round(dx,3))+" "+str(round(dz,3))+" "+str(round(d,3))+"\n")
	fileout.flush()
	textScreen.message(str(samplenbr)+" "+str(round(t-tstart,3))+" "+str(round(dx,3))+" "+str(round(dz,3))+" "+str(round(d,3)))
	if samplenbr==nbrConditions:
		#viz.callback(viz.KEYDOWN_EVENT,None)
		vizinput.message("The file "+fileout.name+" is saved")
		fileout.close()
		startSession()
	else:
		tf.setEnabled(viz.ON)#enable anti-bouncing timer
		

def onTimer():#anti-bouncing delay
	tf.setEnabled(viz.OFF)
	viz.callback(MyPhidgetsButtons.PHIDGETS_BUTTON_PRESSED,buttonPressed)

#other functions
def startSession():
	global fileout
	global samplenbr
	global tstart
	tstart=time.time()
	samplenbr=0;
	participant=vizinput.input("Participant's identifier: ")
	fileout = open(path+"/"+participant+time.strftime('%y%m%d%H%M%S',time.localtime())+".pos", 'w') # outputfile
	viz.callback(MyPhidgetsButtons.PHIDGETS_BUTTON_PRESSED,buttonPressed)
	viz.callback(viz.KEYDOWN_EVENT,onKeyDown) 
	textScreen.message("Next session is started for participant "+participant)
	

def initialize():
	global robot
	global nbrConditions
	global xv
	global zv
	global path
	global tf
	global textScreen
	MyPhidgetsButtons.initPhidgets()
	textScreen = viz.addText('Initializing robotpos',viz.SCREEN)
	textScreen.fontSize(50)
	viz.callback(MyPhidgetsButtons.PHIDGETS_BUTTON_PRESSED,None)
	viz.callback(viz.KEYDOWN_EVENT,None) 
	robot=viztracker.addPos()#position tracker for robot
	vpname=vizinput.fileOpen()
	filein=open(vpname,'r')
	line=filein.readline()
	nbrConditions=int(line)
	xv=range(100)
	zv=range(100)
	for i in range(nbrConditions):
		line=filein.readline()
		s=line.split()
		xv[i]=float(s[0])
		zv[i]=float(s[1])
	filein.close()
	path = vizinput.directory(prompt='Select a folder to save the position files.')
	tf=vizact.ontimer(1.0,onTimer)
	tf.setEnabled(viz.OFF)#disable it for now
	
# main program
initialize()
startSession()
viz.go()